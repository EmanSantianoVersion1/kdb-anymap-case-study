# kdb-anymap-case-study



## Getting started
Run the following command to clone this repo locally:

```
git clone https://gitlab.com/EmanSantianoVersion1/kdb-anymap-case-study.git
```

To generate the on-disk CMO database run the following command in your project directory:


```
q genTable.q
```

Once that has run you should see the anymapDB saved to disk in your project directory. 
To load this is and perform queries on the data run the following from your q/kdb+ sessions:

```
\l anymapDB
```

