tabCount:10000;
maxBagCount:1000;
chunkCount:100;
dates:2020.09.14+til 5;
.cmo.ids:"cmo_",/:string neg[tabCount]?tabCount;           // generate unique CMO ids

// Date functions to get some sensible data
genDates:{[sd;ed] sd+til(ed-sd)+1};
pastDates:genDates[first[dates]-365*20;first[dates]];      // lookback 20yrs
futureDates:genDates[first[dates];first[dates]+365*20];    // forward 20yrs
ignoreNulls:{x where not ""~/:x};

// Base CMO ref table
cmoRefGen:{[n]
    tabDict:()!();
    tabDict[`id]:n?.cmo.ids;
    .cmo.ids:.cmo.ids except tabDict[`id];                 // drop CMO ids as they are used
    tabDict[`currency]:n?`EUR`GBP`USD;
    tabDict[`dayCount]:n?(`ACT360;`ACT365;`30360);
    tabDict[`source]:n?`INTEX`TREPP`PARAGON`EMBS;
    tabDict[`rating]:n?raze[`$(1 2 3)#\:/:("ABC")],`D;
    :flip tabDict;};

// Function to create a single mortgage dictionary 
genCollBag:{[]
    bagDict:()!();
    bagDict[`secId]:raze(3?.Q.A),string 1?100+til 500;
    bagDict[`pool]:first 1?(1+til 7);
    bagDict[`rating]:first 1?raze[`$(1 2 3)#\:/:("ABC")],`D;
    bagDict[`isBalloon]:first 1?`boolean$0 1;
    bagDict[`loanAge]:first 1?28;
    bagDict[`originalAmount]:first 1000*70+1?700;
    bagDict[`source]:first 1?`INTEX`TREPP`PARAGON`EMBS;
    bagDict[`coupon]:first 2+1?6f;
    bagDict[`propertyType]:first 1?`SNGL`MULTI;
    bagDict[`description]:" " sv first each 1?'(("CUSTOM";"SF");("5-YEAR";"30YR");
                                                ("HYBRID";"JUMBO-CONFORMING";"BUY-DOWN");("ARM 1YR CMT 1/5 CAP";""));
    bagDict[`originator]:first 1?`Broker`Correspondent`Retail`Unknown;
    bagDict[`currency]:first 1?`EUR`GBP`USD;
    bagDict[`dayCount]:first 1?(`ACT360;`ACT365;`30360);
    bagDict[`occupancyType]:first 1?`OwnerOccupied`SecondHome`InvestmentHome`Other;
    bagDict[`delinquencyMonths]:first 1?til 5;
    bagDict[`armCap]:first 1?`$("1/5";"2/5";"2/6");
    bagDict[`armFloor]:first 1?100f;

    // Calc these fields from previous fields
    bagDict[`delinquencyStatus]:first (til[5]!`$("";"30+";"60+";"90+";"120+"))[bagDict`delinquencyMonths];
    bagDict[`state]:(`EUR`GBP`USD!(`;`;first 1?`NY`NJ`CA`FL))[bagDict`currency];
    bagDict[`currentAmount]:bagDict[`originalAmount]-bagDict[`loanAge]*3*1000;
    bagDict[`remainingTerm]:30-bagDict[`loanAge];
    bagDict[`maturityDate]:.Q.addmonths[first[dates];12*bagDict[`remainingTerm]];
    bagDict[`loanToValue]:100*bagDict[`currentAmount]%{x+0.2*x}bagDict[`originalAmount];
    bagDict[`originalTerm]:first bagDict[`loanAge]+1?12;
    bagDict[`appraisalDate]:min pastDates where pastDates within (bagDict[`maturityDate]
                                                                 -(40-bagDict[`loanAge])*365;first[dates]);
    bagDict[`modificationDate]:first 1?pastDates where pastDates within (bagDict[`maturityDate]
                                                                 -bagDict[`loanAge]*365;first[dates]);

    // Drop some keys to randomize the dicts, but keep some common keys
    :(10?key[bagDict] except `secId`pool`rating`coupon`currentAmount`originalTerm`remainingTerm)_ bagDict;};

    // Create multiple mortgage dictionaries per row & add to res as the collBag (anymap) column
genManyCollBags:{[n;fn] 1_ n fn\`}[;genCollBag];

updTabAvgs:{[t]
    update grossCoupon:avg each ignoreNulls'[collBag[;;`coupon]],
        currentAmount:avg each ignoreNulls'[collBag[;;`currentAmount]],
        maturityDate:`date$avg each ignoreNulls'[collBag[;;`maturityDate]],
        loanAge:floor avg each ignoreNulls'[collBag[;;`loanAge]],
        loanToValue:avg each ignoreNulls'[collBag[;;`loanToValue]],
        originalAmount:avg each ignoreNulls'[collBag[;;`originalAmount]],
        originalTerm:floor avg each ignoreNulls'[collBag[;;`originalTerm]],
        wam:sum each collBag[;;`remainingTerm]*{x%sum[x]}'[collBag[;;`currentAmount]]
    from t};

write:{[dt;tab;db] hsym[`$"/" sv (db;string dt;string tab;())] set .Q.en[hsym`$db;eval tab];}

updAndWrite:{[n]
    cb:genManyCollBags each n;
    cmoRef::updTabAvgs[update collBag:cb from cmoRefGen[chunkCount]];
    write[;`cmoRef;"anymapDB"] each dates;};

main:{[]
    chunks:chunkCount cut 2+tabCount?maxBagCount;                  // ensure at least 2 mortgages per pool
    updAndWrite each chunks;
    {p 1: get p:hsym[`$"/" sv ("anymapDB";x;"cmoRef";"collBag")]} each string dates;  // apply type anymap
    exit 0;};

main[];